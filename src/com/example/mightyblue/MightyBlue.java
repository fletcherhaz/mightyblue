package com.example.mightyblue;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import com.example.mightyblue.fragments.ChargingFragment;
import com.example.mightyblue.fragments.DischargingFragment;
import com.example.mightyblue.fragments.DrivingFragment;
import com.example.mightyblue.fragments.MightyBlueFragment;
import com.example.mightyblue.fragments.SettingsFragment;
import com.example.mightyblue.fragments.StatusFragment;
import com.example.mightyblue.services.CanbusUpdaterService;
import com.example.mightyblue.services.GPSUpdaterService;

//add support to variable class to keep 
//When serial io manager stops. USER NEEEDS TO KNOW Use Toast
//dtc checks for bms status
//look at viewpager to get some more animations for switching fragments

public class MightyBlue extends Activity implements ActionBar.TabListener {
	
    private final String TAG = MightyBlue.class.getSimpleName();
    private ActionBar actionBar;
    private String fragmentTag = "";
    private GestureDetector mDetector;
    private int selectedTab = 1;
    private Intent canbusService;
    private Intent gpsService;

    @Override
    public void onCreate(Bundle savedInstanceState) {
    	//start the service
    	super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        canbusService = new Intent(this, CanbusUpdaterService.class);
        gpsService = new Intent(this, GPSUpdaterService.class);
        startService(canbusService);
        SharedPreferences prefs = getSharedPreferences("MightyBlueGPS", Context.MODE_PRIVATE);
        if(prefs.getBoolean("GPSSetting", true))
        {
        	boolean enabled = ((LocationManager) getSystemService(Context.LOCATION_SERVICE)).isProviderEnabled(LocationManager.GPS_PROVIDER);
    		if (!enabled) {
    		  Intent intent2 = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
    		  startActivity(intent2);
    		  enabled = ((LocationManager) getSystemService(Context.LOCATION_SERVICE)).isProviderEnabled(LocationManager.GPS_PROVIDER);
    		}
    		if(enabled)
    		{
    			startService(gpsService);
    		}
        }

        actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        actionBar.addTab(actionBar.newTab()
        		.setText(R.string.title_driving)
        		.setTabListener(this));
        actionBar.addTab(actionBar.newTab()
        		.setText(R.string.title_status)
        		.setTabListener(this));
        actionBar.addTab(actionBar.newTab()
        		.setText(R.string.title_discharging)
        		.setTabListener(this));
        actionBar.addTab(actionBar.newTab()
        		.setText(R.string.title_charging)
        		.setTabListener(this));
        actionBar.addTab(actionBar.newTab()
        		.setText(R.string.title_settings)
        		.setTabListener(this));

        mDetector = new GestureDetector(this, new MyGestureListener());
    }

	@Override
	protected void onDestroy() {
        super.onDestroy();
        stopService(canbusService);
        stopService(gpsService);
	}

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    	FragmentManager fm = getFragmentManager();
    	if(!fragmentTag.equals("settings"))
    	{
    		MightyBlueFragment oldFragment = (MightyBlueFragment) fm.findFragmentByTag(fragmentTag);
    		if(oldFragment != null)
    		{
    			oldFragment.cancel();
    			while(!oldFragment.isCancelled())
    				try {Thread.sleep(50);}
    				catch (InterruptedException e) {e.printStackTrace();}
    		}
    	}
    	Fragment newFragment;
    	switch(tab.getPosition())
    	{
    		case 1:
    			newFragment = new StatusFragment();
    			fragmentTag = "status";
    			break;
    		case 2:
    			newFragment = new DischargingFragment();
    			fragmentTag = "discharging";
    			break;
    		case 3:
    			newFragment = new ChargingFragment();
    			fragmentTag = "charging";
    			break;
    		case 4:
    			newFragment = new SettingsFragment();
    			fragmentTag = "settings";
    			break;
    		default:
    			newFragment = new DrivingFragment();
    			fragmentTag = "driving";
    			break;
    	}
    	if(actionBar.getSelectedNavigationIndex() < selectedTab)
    		fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_right);
    	else
    		fragmentTransaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_left);
    	selectedTab = actionBar.getSelectedNavigationIndex();
    	fragmentTransaction.replace(R.id.main_layout, newFragment, fragmentTag);
    	//transaction.addToBackStack(null);
    	//fragmentTransaction.commit();
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {}
    
	@Override
	public void onTabUnselected(Tab arg0, FragmentTransaction arg1) {}

    @Override 
    public boolean onTouchEvent(MotionEvent event){ 
        this.mDetector.onTouchEvent(event);
        return super.onTouchEvent(event);
    }

    public void handleDrivingClick(View view)
	{
    	FragmentManager fm = getFragmentManager();
		DrivingFragment f = (DrivingFragment) fm.findFragmentByTag("driving");
		f.handleClick(view);
	}

	class MyGestureListener extends GestureDetector.SimpleOnGestureListener {
		private static final int SWIPE_MIN_DISTANCE = 50;
	    private static final int SWIPE_MAX_OFF_PATH = 200;
	    private static final int SWIPE_THRESHOLD_VELOCITY = 200;
        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        	float diffAbs = Math.abs(e1.getY() - e2.getY());
            float diff = e1.getX() - e2.getX();
            int selectedNavigationIndex = actionBar.getSelectedNavigationIndex();

            if (diffAbs > SWIPE_MAX_OFF_PATH)
                return false;

            // Left swipe
            if (diff > SWIPE_MIN_DISTANCE
                    && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
            	if(selectedNavigationIndex != 4)
            		actionBar.setSelectedNavigationItem(selectedNavigationIndex+1);
            } 
            // Right swipe
            else if (-diff > SWIPE_MIN_DISTANCE
                    && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
            	if(selectedNavigationIndex != 0)
            		actionBar.setSelectedNavigationItem(selectedNavigationIndex-1);
            }
            return true;
        }
    }
}
