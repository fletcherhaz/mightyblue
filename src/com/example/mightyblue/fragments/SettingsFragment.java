package com.example.mightyblue.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.mightyblue.R;
import com.example.mightyblue.adapters.SettingsAdapter;

public class SettingsFragment extends MightyBlueFragment
{
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View v = inflater.inflate(R.layout.settings, container, false);
		ListView list = (ListView) v.findViewById(R.id.settings_list);
//		list.setOnItemClickListener(new AdapterView.OnItemClickListener()
//		{
//			@Override
//			public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
//				String item = (String) parent.getItemAtPosition(position);
//			}
//		});
		String[] names = new String[] {"GPS", "Sounds", "Logging"};
		SettingsAdapter adapter = new SettingsAdapter(v.getContext(), R.layout.settingsrow, names);
	    list.setAdapter(adapter);
    	return v;
    }

}
