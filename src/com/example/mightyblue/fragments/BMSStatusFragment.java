package com.example.mightyblue.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.example.mightyblue.CanbusVariables;
import com.example.mightyblue.R;
import com.example.mightyblue.services.CanbusServiceConnection;

public class BMSStatusFragment extends MightyBlueStatusFragment
{
	private TextView bmsStatusCharging;
	private TextView bmsStatusReady;
	UpdateUI updater;

	@Override
	public Dialog onCreateDialog(final Bundle savedInstanceState)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("BMS Status");
        builder.setPositiveButton("Return", new DialogInterface.OnClickListener() {
               public void onClick(DialogInterface dialog, int id) {
            	   BMSStatusFragment.this.cancel();
                   dialog.dismiss();
                   ((DrivingFragment) getTargetFragment()).createNewUpdater();
               }
           });
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.bmsstatus, null);
		builder.setView(v);
    	Typeface type = Typeface.createFromAsset(getActivity().getAssets(),"Digital7.ttf");
    	bmsStatusReady = (TextView) v.findViewById(R.id.bms_status_ready);
    	bmsStatusCharging = (TextView) v.findViewById(R.id.bms_status_charging);
		bmsStatusReady.setTypeface(type);
		bmsStatusCharging.setTypeface(type);
		return builder.create();
	}

	@Override
	public void createNewUpdater()
	{
		updater = new UpdateUI()
		{
			@Override
			protected void onProgressUpdate(CanbusServiceConnection... connection)
			{
				if(!mConnection.isBound())
					return;
				CanbusVariables vars = connection[0].getService().getVariables();
				if (vars.findVariable("B_IS_READY").getValue() == 1) {
					bmsStatusReady.setText("Yes");
					bmsStatusReady.setTextColor(getActivity().getResources().getColor(R.color.neon_green));
				} else {
					bmsStatusReady.setText("No");
					bmsStatusReady.setTextColor(getActivity().getResources().getColor(R.color.bright_red));
				}
				if (vars.findVariable("B_IS_CHARGING").getValue() == 1) {
					bmsStatusCharging.setText("Yes");
					bmsStatusCharging.setTextColor(getActivity().getResources().getColor(R.color.neon_green));
				} else {
					bmsStatusCharging.setText("No");
					bmsStatusCharging.setTextColor(getActivity().getResources().getColor(R.color.bright_red));
				}
			}
		};
		updater.execute();
	}

	@Override
	public void cancel()
	{
		if(!updater.isCancelled())
			updater.cancel(true);
	}

	@Override
	public boolean isCancelled()
	{
		return updater.isCancelled();
	}
}
