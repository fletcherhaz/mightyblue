package com.example.mightyblue.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.example.mightyblue.CanbusVariables;
import com.example.mightyblue.R;
import com.example.mightyblue.adapters.ControllerDTCAdapter;
import com.example.mightyblue.services.CanbusServiceConnection;

public class ControllerStatusFragment extends MightyBlueStatusFragment
{
	private UpdateUI updater;
	private ControllerDTCAdapter adapter;
	private TextView controllerStatusState;
	private TextView controllerStatusChillplate;
	private TextView controllerStatusFilmcap;

	@Override
	public Dialog onCreateDialog(final Bundle savedInstanceState)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		String[] names = new String[22];
		for(int i=0; i<22; ++i)
		{
			names[i] = "Motor DTC " + (i+1);
		}
		LayoutInflater inflater = getActivity().getLayoutInflater();
		View v = inflater.inflate(R.layout.controllerstatus, null);
		adapter = new ControllerDTCAdapter(getActivity(), R.layout.controllerdtcrow, names);
		((ListView) v.findViewById(R.id.controller_status_dtc_list)).setAdapter(adapter);
		builder.setView(v);
		builder.setTitle("Controller Status");
        builder.setPositiveButton("Return", new DialogInterface.OnClickListener() {
               public void onClick(DialogInterface dialog, int id) {
            	   ControllerStatusFragment.this.cancel();
                   dialog.dismiss();
                   ((DrivingFragment) getTargetFragment()).createNewUpdater();
               }
           });
    	Typeface type = Typeface.createFromAsset(getActivity().getAssets(),"Digital7.ttf");
    	controllerStatusState = (TextView) v.findViewById(R.id.controller_status_state);
    	controllerStatusChillplate = (TextView) v.findViewById(R.id.controller_status_chillplate);
    	controllerStatusFilmcap = (TextView) v.findViewById(R.id.controller_status_filmcap);
    	controllerStatusState.setTypeface(type);
    	controllerStatusChillplate.setTypeface(type);
    	controllerStatusFilmcap.setTypeface(type);
    	controllerStatusChillplate.setTextColor(getActivity().getResources().getColor(R.color.neon_green));
    	controllerStatusFilmcap.setTextColor(getActivity().getResources().getColor(R.color.neon_green));
		return builder.create();
	}
	
	@Override
	public void createNewUpdater()
	{
		updater = new UpdateUI()
		{
			@Override
			protected void onProgressUpdate(CanbusServiceConnection... connection)
			{
				getDialog().getWindow().setLayout(1200, 600);
				if(!mConnection.isBound())
					return;
				CanbusVariables vars = connection[0].getService().getVariables();
				adapter.updateDTCs(vars);
				double state = vars.findVariable("M_CONTROLLER_STATE").getValue();
				if(state == 0)
				{
					controllerStatusState.setText("Key Off");
					controllerStatusState.setTextColor(getActivity().getResources().getColor(R.color.neon_green));
				}
				else if(state == 1)
				{
					controllerStatusState.setText("Key On");
					controllerStatusState.setTextColor(getActivity().getResources().getColor(R.color.neon_green));
				}
				else if(state == 2)
				{
					controllerStatusState.setText("Started");
					controllerStatusState.setTextColor(getActivity().getResources().getColor(R.color.neon_green));
				}
				else
				{
					controllerStatusState.setText("No Comm");
					controllerStatusState.setTextColor(getActivity().getResources().getColor(R.color.bright_yellow));
				}
				controllerStatusChillplate.setText(vars.findVariable("M_CHILLPLATE_TEMP").getValue() + "");
			}
		};
		updater.execute();
	}

	@Override
	public void cancel()
	{
		if(!updater.isCancelled())
			updater.cancel(true);
	}

	@Override
	public boolean isCancelled()
	{
		return updater.isCancelled();
	}
}
