package com.example.mightyblue.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.mightyblue.CanbusVariables;
import com.example.mightyblue.R;
import com.example.mightyblue.services.CanbusServiceConnection;

public class ChargingFragment extends MightyBlueFragment
{
	TextView bmsAmpsValue;
	TextView bmsAmpsMax;
	TextView bmsVoltsValue;
	TextView bmsVoltsMax;
	TextView chargerAmpsValue;
	TextView chargerAmpsMax;
	TextView chargerVoltsValue;
	TextView chargerVoltsMax;
	TextView chargerStatus;
	TextView bmsStatus;
	UpdateUI updater;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View v = inflater.inflate(R.layout.charging, container, false);
    	bmsAmpsValue = (TextView) v.findViewById(R.id.charging_bms_amps_value);
    	bmsAmpsMax = (TextView) v.findViewById(R.id.charging_bms_amps_max);
    	bmsVoltsValue = (TextView) v.findViewById(R.id.charging_bms_volts_value);
    	bmsVoltsMax = (TextView) v.findViewById(R.id.charging_bms_volts_max);
    	chargerAmpsValue = (TextView) v.findViewById(R.id.charging_charger_amps_value);
    	chargerAmpsMax = (TextView) v.findViewById(R.id.charging_charger_amps_max);
    	chargerVoltsValue = (TextView) v.findViewById(R.id.charging_charger_volts_value);
    	chargerVoltsMax = (TextView) v.findViewById(R.id.charging_charger_volts_max);
    	chargerStatus = (TextView) v.findViewById(R.id.charging_charger_status);
    	bmsStatus = (TextView) v.findViewById(R.id.charging_bms_status);
    	Typeface type = Typeface.createFromAsset(container.getContext().getAssets(),"Digital7.ttf");
    	bmsAmpsValue.setTypeface(type);
    	bmsAmpsMax.setTypeface(type);
    	bmsVoltsValue.setTypeface(type);
    	bmsVoltsMax.setTypeface(type);
    	chargerAmpsValue.setTypeface(type);
    	chargerAmpsMax.setTypeface(type);
    	chargerVoltsValue.setTypeface(type);
    	chargerVoltsMax.setTypeface(type);
    	chargerStatus.setTypeface(type);
		return v;
	}

	@Override
	public void createNewUpdater()
	{
		updater = new UpdateUI()
		{
			@Override
			protected void onProgressUpdate(CanbusServiceConnection... connection)
			{
				if(!mConnection.isBound())
					return;
				CanbusVariables vars = connection[0].getService().getVariables();
				bmsAmpsValue.setText(vars.findVariable("B_PACK_CURRENT").getValue() + "");
				//bmsAmpsMax.setText(vars.findVariable("B_CCL_FOR_ELCON").getValue() + "");
				bmsVoltsValue.setText(vars.findVariable("B_PACK_VOLTAGE").getValue() + "");
				//bmsVoltsMax.setText(vars.findVariable("B_VOLTAGE_FOR_ELCON").getValue() + "");
				chargerAmpsValue.setText(vars.findVariable("C_CURRENT").getValue() + "");
				chargerVoltsValue.setText(vars.findVariable("C_VOLTAGE").getValue() + "");
				if (vars.findVariable("B_CONTROL_FOR_ELCON").getValue() == 1)
				{
					if (vars.findVariable("C_HARDWARE_FAILURE").getValue() == 1 || vars.findVariable("C_TEMPERATURE").getValue() == 1 ||
						vars.findVariable("C_INPUT_VOLTAGE").getValue() == 1 || vars.findVariable("C_STATING_STATE").getValue() == 1 ||
						vars.findVariable("C_COMM_STATE").getValue() == 1) {
						chargerStatus.setText("ERROR");
						chargerStatus.setTextColor(getActivity().getResources().getColor(R.color.bright_red));
					}
					else
					{
						chargerStatus.setText("ALL GOOD");
						chargerStatus.setTextColor(getActivity().getResources().getColor(R.color.neon_green));
					}
				}
				else
				{
					chargerStatus.setText("---");
					chargerStatus.setTextColor(getActivity().getResources().getColor(R.color.dull_gray));
				}
				if (vars.findVariable("B_IS_READY").getValue() == 0) {
					bmsStatus.setText("BMS ERROR");
					bmsStatus.setTextColor(getActivity().getResources().getColor(R.color.bright_red));
				} else {
					bmsStatus.setText("BMS GOOD");
					bmsStatus.setTextColor(getActivity().getResources().getColor(R.color.neon_green));
				}
			}
		};
		updater.setDelayMultiplier(10);
		updater.setMessages(new String[] {"t7E340322F00C", "t7E340322F00D"});
		updater.execute();
	}

	@Override
	public void cancel()
	{
		if(!updater.isCancelled())
			updater.cancel(true);
	}

	@Override
	public boolean isCancelled()
	{
		return updater.isCancelled();
	}
}
