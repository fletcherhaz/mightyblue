package com.example.mightyblue.fragments;

import java.math.RoundingMode;
import java.text.DecimalFormat;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.example.mightyblue.R;
import com.example.mightyblue.services.CanbusServiceConnection;

public class GPSStatusFragment extends MightyBlueStatusFragment
{
	private TextView gpsStatusTotal;
	private TextView gpsStatusTrip;
	private SharedPreferences prefs;
	UpdateUI updater;

	@Override
	public Dialog onCreateDialog(final Bundle savedInstanceState)
	{
		prefs = getActivity().getSharedPreferences("MightyBlueGPS", Context.MODE_PRIVATE);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("GPS Status");
        builder.setPositiveButton("Return", new DialogInterface.OnClickListener() {
               public void onClick(DialogInterface dialog, int id) {
            	   GPSStatusFragment.this.cancel();
                   dialog.dismiss();
                   ((DrivingFragment) getTargetFragment()).createNewUpdater();
               }
           });
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.gpsstatus, null);
		builder.setView(v);
		((Button) v.findViewById(R.id.gps_status_trip_miles_reset))
		.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				prefs.edit().putFloat("TripMiles", 0).apply();
			}
		});
    	Typeface type = Typeface.createFromAsset(getActivity().getAssets(),"Digital7.ttf");
    	gpsStatusTotal = (TextView) v.findViewById(R.id.gps_status_total_miles);
    	gpsStatusTrip = (TextView) v.findViewById(R.id.gps_status_trip_miles);
		gpsStatusTotal.setTypeface(type);
		gpsStatusTrip.setTypeface(type);
		return builder.create();
	}

	@Override
	public void createNewUpdater()
	{
		updater = new UpdateUI()
		{
			DecimalFormat df = new DecimalFormat("#.#");
			@Override
			protected void onPreExecute()
			{
				df.setRoundingMode(RoundingMode.DOWN);
			}

			@Override
			protected void onProgressUpdate(CanbusServiceConnection... connection)
			{
				Log.i("GPS Status Fragment", "PROGRESS" + prefs.getLong("TotalMiles", 0));
				gpsStatusTotal.setText(prefs.getLong("TotalMiles", 0) + "");
				gpsStatusTrip.setText(df.format(prefs.getFloat("TripMiles", 0)) + "");
			}
		};
		updater.setSleepTime(500);
		updater.execute();
	}

	@Override
	public void cancel()
	{
		if(!updater.isCancelled())
			updater.cancel(true);
	}

	@Override
	public boolean isCancelled()
	{
		return updater.isCancelled();
	}

}
