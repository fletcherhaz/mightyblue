package com.example.mightyblue.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.mightyblue.CanbusVariables;
import com.example.mightyblue.R;
import com.example.mightyblue.services.CanbusServiceConnection;

public class DischargingFragment extends MightyBlueFragment
{
	TextView bmsAmpsValue;
	TextView bmsAmpsMax;
	TextView bmsVoltsValue;
	TextView bmsVoltsMax;
	TextView controllerAmpsValue;
	TextView controllerAmpsMax;
	TextView controllerVoltsValue;
	TextView controllerVoltsMax;
	UpdateUI updater;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View v = inflater.inflate(R.layout.discharging, container, false);
    	bmsAmpsValue = (TextView) v.findViewById(R.id.discharging_bms_amps_value);
    	bmsAmpsMax = (TextView) v.findViewById(R.id.discharging_bms_amps_max);
    	bmsVoltsValue = (TextView) v.findViewById(R.id.discharging_bms_volts_value);
    	bmsVoltsMax = (TextView) v.findViewById(R.id.discharging_bms_volts_max);
    	controllerAmpsValue = (TextView) v.findViewById(R.id.discharging_motor_amps_value);
    	controllerAmpsMax = (TextView) v.findViewById(R.id.discharging_motor_amps_max);
    	controllerVoltsValue = (TextView) v.findViewById(R.id.discharging_motor_volts_value);
    	controllerVoltsMax = (TextView) v.findViewById(R.id.discharging_motor_volts_max);
    	Typeface type = Typeface.createFromAsset(container.getContext().getAssets(),"Digital7.ttf");
    	bmsAmpsValue.setTypeface(type);
    	bmsAmpsMax.setTypeface(type);
    	bmsVoltsValue.setTypeface(type);
    	bmsVoltsMax.setTypeface(type);
    	controllerAmpsValue.setTypeface(type);
    	controllerAmpsMax.setTypeface(type);
    	controllerVoltsValue.setTypeface(type);
    	controllerVoltsMax.setTypeface(type);
		return v;
	}

	@Override
	public void createNewUpdater()
	{
		updater = new UpdateUI()
		{
			@Override
			protected void onProgressUpdate(CanbusServiceConnection... connection)
			{
				if(!mConnection.isBound())
					return;
				CanbusVariables vars = connection[0].getService().getVariables();
				bmsAmpsValue.setText(vars.findVariable("B_PACK_CURRENT").getValue() + "");
				bmsAmpsMax.setText(vars.findVariable("B_DCL").getValue() + "");
				bmsVoltsValue.setText(vars.findVariable("B_PACK_VOLTAGE").getValue() + "");
				//textViews.get("discharging_bms_volts_max").setText("---");
				controllerAmpsValue.setText(vars.findVariable("C_MAMPS").getValue() + "");
				controllerAmpsMax.setText(vars.findVariable("C_CLMF").getValue() + "");
				controllerVoltsValue.setText(vars.findVariable("C_BVOLTS").getValue() + "");
				controllerVoltsMax.setText(vars.findVariable("C_VLMF").getValue() + "");
			}
		};
		updater.execute();
	}

	@Override
	public void cancel()
	{
		if(!updater.isCancelled())
			updater.cancel(true);
	}

	@Override
	public boolean isCancelled()
	{
		return updater.isCancelled();
	}
}
