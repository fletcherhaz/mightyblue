package com.example.mightyblue.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.mightyblue.CanbusVariables;
import com.example.mightyblue.R;
import com.example.mightyblue.services.CanbusServiceConnection;

public class ChargerStatusFragment extends MightyBlueFragment
{
	private UpdateUI updater;
	private TextView hardware;
	private TextView temp;
	private TextView voltage;
	private TextView state;
	private TextView comm;
	private Typeface type;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View v = inflater.inflate(R.layout.chargerstatus, container, false);
    	hardware = (TextView) v.findViewById(R.id.charger_status_hardware);
    	temp = (TextView) v.findViewById(R.id.charger_status_temp);
    	voltage = (TextView) v.findViewById(R.id.charger_status_voltage);
    	state = (TextView) v.findViewById(R.id.charger_status_state);
    	comm = (TextView) v.findViewById(R.id.charger_status_comm);
    	type = Typeface.createFromAsset(container.getContext().getAssets(),"Digital7.ttf");
    	hardware.setTypeface(type);
    	temp.setTypeface(type);
    	voltage.setTypeface(type);
    	state.setTypeface(type);
    	comm.setTypeface(type);
		return v;
	}

	@Override
	public void createNewUpdater()
	{
		updater = new UpdateUI()
		{
			@Override
			protected void onProgressUpdate(CanbusServiceConnection... connection)
			{
				if(!mConnection.isBound())
					return;
				CanbusVariables vars = connection[0].getService().getVariables();
				if (vars.findVariable("C_HARDWARE_FAILURE").getValue() == 0) {
					hardware.setText("Normal");
					hardware.setTextColor(getActivity().getResources().getColor(R.color.neon_green));
				} else {
					hardware.setText("Hardware failure");
					hardware.setTextColor(getActivity().getResources().getColor(R.color.bright_red));
				}
				if (vars.findVariable("C_TEMPERATURE").getValue() == 0) {
					temp.setText("Normal");
					temp.setTextColor(getActivity().getResources().getColor(R.color.neon_green));
				} else {
					temp.setText("Over Temperature Protection");
					temp.setTextColor(getActivity().getResources().getColor(R.color.bright_red));
				}
				if (vars.findVariable("C_INPUT_VOLTAGE").getValue() == 0) {
					voltage.setText("Normal");
					voltage.setTextColor(getActivity().getResources().getColor(R.color.neon_green));
				} else {
					voltage.setText("Wrong. Charger Stopped");
					voltage.setTextColor(getActivity().getResources().getColor(R.color.bright_red));
				}
				if (vars.findVariable("C_STATING_STATE").getValue() == 0) {
					state.setText("Entered Starting State");
					state.setTextColor(getActivity().getResources().getColor(R.color.neon_green));
				} else {
					state.setText("Charger closed.");
					state.setTextColor(getActivity().getResources().getColor(R.color.bright_red));
				}
				if (vars.findVariable("C_COMM_STATE").getValue() == 0) {
					comm.setText("Normal");
					comm.setTextColor(getActivity().getResources().getColor(R.color.neon_green));
				} else {
					comm.setText("Communication Failure");
					comm.setTextColor(getActivity().getResources().getColor(R.color.bright_red));
				}
			}
		};
		updater.execute();
	}

	@Override
	public void cancel()
	{
		if(!updater.isCancelled())
			updater.cancel(true);
	}

	@Override
	public boolean isCancelled()
	{
		return updater.isCancelled();
	}
}
