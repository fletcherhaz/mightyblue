package com.example.mightyblue.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.mightyblue.CanbusVariables;
import com.example.mightyblue.R;
import com.example.mightyblue.services.CanbusServiceConnection;
import com.example.mightyblue.services.GPSServiceConnection;
import com.example.mightyblue.services.GPSUpdaterService;

public class DrivingFragment extends MightyBlueFragment
{
	private TextView amps;
	private TextView volts;
	private TextView soc;
	private TextView reverse;
	private TextView fbrake;
	private TextView bms;
	private TextView controller;
	private TextView canbus;
	private TextView gps;
	private ImageView needle;
	private ProgressBar socbar;
	private UpdateUI updater; 
	private GPSServiceConnection gpsConnection = new GPSServiceConnection();

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View v = inflater.inflate(R.layout.driving, container, false);
		socbar = (ProgressBar) v.findViewById(R.id.driving_soc_bar);
		ImageView speed = (ImageView) v.findViewById(R.id.driving_speedometer);
		speed.setImageResource(R.drawable.speedometer);
    	needle = (ImageView) v.findViewById(R.id.driving_speedometer_needle);
    	needle.setImageResource(R.drawable.speedometer_needle);
        Matrix mat = new Matrix();
        mat.postRotate((float) 155, 0, 69);
        needle.setScaleType(ScaleType.MATRIX);
        needle.setImageMatrix(mat);
        needle.setPadding(853, 197, 0, 0);
    	amps = (TextView) v.findViewById(R.id.driving_amps);
    	volts = (TextView) v.findViewById(R.id.driving_volts);
    	soc = (TextView) v.findViewById(R.id.driving_soc);
    	reverse = (TextView) v.findViewById(R.id.driving_reverse);
    	fbrake = (TextView) v.findViewById(R.id.driving_foot_brake);
    	bms = (TextView) v.findViewById(R.id.driving_bms_status);
    	controller = (TextView) v.findViewById(R.id.driving_controller_status);
    	canbus = (TextView) v.findViewById(R.id.driving_canbus_status);
    	gps = (TextView) v.findViewById(R.id.driving_gps_status);
    	Typeface type = Typeface.createFromAsset(getActivity().getAssets(),"Digital7.ttf");
    	amps.setTypeface(type);
    	volts.setTypeface(type);
    	soc.setTypeface(type);
    	reverse.setTypeface(type, 1);
    	fbrake.setTypeface(type, 1);
    	bms.setTypeface(type, 1);
    	controller.setTypeface(type, 1);
    	canbus.setTypeface(type, 1);
    	gps.setTypeface(type, 1);
    	return v;
    }

	@Override
	public void onResume()
	{
		Intent intent = new Intent(getActivity(), GPSUpdaterService.class);
		getActivity().bindService(intent, gpsConnection, Context.BIND_AUTO_CREATE);
		super.onResume();
	}

	@Override
	public void onPause()
	{
        if (gpsConnection.isBound())
            getActivity().unbindService(gpsConnection);
        super.onPause();
	}

	@Override
	public void createNewUpdater()
	{
		//Old canbus message String mesg = "x0CFF030110C" + "\015" + "O" + "\015";
		updater = new UpdateUI()
		{
			@Override
			protected void onProgressUpdate(CanbusServiceConnection... connection)
			{
				if(!mConnection.isBound())
				{
					canbus.setTextColor(getResources().getColor(R.color.bright_red));
					canbus.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.canbus_red, 0, 0);
					return;
				}
				CanbusVariables vars = connection[0].getService().getVariables();
		        Matrix mat = new Matrix();
		        String speed = "0";
		        if(gpsConnection.isBound())
		        	speed = gpsConnection.getService().getSpeed();
		        mat.postRotate((float) (Double.parseDouble(speed)*1.93+155), 0, 69);
		        needle.setScaleType(ScaleType.MATRIX);
		        needle.setImageMatrix(mat);
		        needle.setPadding(853, 197, 0, 0);
				amps.setText(vars.findVariable("B_PACK_CURRENT").getValue() + "");
				volts.setText(vars.findVariable("B_PACK_VOLTAGE").getValue() + "");
				soc.setText(vars.findVariable("B_PACK_SOC").getValue() + " %");
				socbar.setProgress((int) vars.findVariable("B_PACK_SOC").getValue());
				if (vars.findVariable("C_REVERSE_INPUT").getValue() == 0) {
					reverse.setTextColor(getResources().getColor(R.color.dull_gray));
					reverse.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
					reverse.setPadding(150, 150, 0, 0);
				} else {
					reverse.setTextColor(getResources().getColor(R.color.neon_pink));
					reverse.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.reverse, 0, 0);
					reverse.setPadding(140, 20, 0, 0);
				}
				if (vars.findVariable("C_BRAKE_INPUT").getValue() == 0) {
					fbrake.setTextColor(getResources().getColor(R.color.dull_gray));
					fbrake.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
					fbrake.setPadding(70, 150, 0, 0);
				} else {
					fbrake.setTextColor(getResources().getColor(R.color.neon_pink));
					fbrake.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.brake, 0, 0);
					fbrake.setPadding(70, 20, 0, 0);
				}
		/*
				ProgressBar throttle1 = (ProgressBar) v.findViewById(R.id.driving_throttle_1);
				ProgressBar throttle2 = (ProgressBar) v.findViewById(R.id.driving_throttle_2);
				String first = Integer.toHexString((int) vars.findVariable("M_THROTTLE_1_HIGH").getValue());
				String second = Integer.toHexString((int) vars.findVariable("M_THROTTLE_1_LOW").getValue());
				first = first.toUpperCase();
				second = second.toUpperCase();
				byte thefirst = (byte) (Integer.parseInt(first, 16) & 0x000000FF);
				byte thesecond = (byte) (Integer.parseInt(second, 16) & 0x000000FF);
				int newval = thefirst & 0xFF;
				newval <<= 8;
				newval |= thesecond & 0xFF;
				//System.out.println("1throttle" + newval);
				throttle1.setProgress(newval);
				//((TextView) v.findViewById(R.id.driving_amps)).setText(String.valueOf(newval));
				first = Integer.toHexString((int) vars.findVariable("M_THROTTLE_2_HIGH").getValue());
				second = Integer.toHexString((int) vars.findVariable("M_THROTTLE_2_LOW").getValue());
				first = first.toUpperCase();
				second = second.toUpperCase();
				thefirst = (byte) (Integer.parseInt(first, 16) & 0x000000FF);
				thesecond = (byte) (Integer.parseInt(second, 16) & 0x000000FF);
				newval = thefirst & 0xFF;
				newval <<= 8;
				newval |= thesecond & 0xFF;
				//voltage = newval;
				//System.out.print(voltage);
				//voltage = (((voltage/1023) * 5.0) - 1.6) / 3.35;
				//System.out.println(Double.toString(voltage));
				//newval = (int) Math.round((voltage*100));
				//System.out.println("2throttle" + newval);
				throttle2.setProgress(newval);*/
		
				double bmsStatus = vars.findVariable("B_IS_READY").getValue();
				if (bmsStatus == 0) {
					bms.setTextColor(getResources().getColor(R.color.bright_red));
					bms.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.bms_red, 0, 0);
				} else if(bmsStatus == -1) {
					bms.setTextColor(getResources().getColor(R.color.bright_yellow));
					bms.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.bms_yellow, 0, 0);
				} else {
					bms.setTextColor(getResources().getColor(R.color.neon_green));
					bms.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.bms_green, 0, 0);
				}
				if(vars.findVariable("C_STATE").getValue() == -1)
				{
					controller.setTextColor(getResources().getColor(R.color.bright_yellow));
					controller.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.controller_yellow, 0, 0);
				}
				else
				{
					controller.setTextColor(getResources().getColor(R.color.neon_green));
					controller.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.controller_green, 0, 0);
				}
				if(!connection[0].getService().usbConnected())
				{
					canbus.setTextColor(getResources().getColor(R.color.bright_yellow));
					canbus.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.canbus_yellow, 0, 0);
				}
				else
				{
					canbus.setTextColor(getResources().getColor(R.color.neon_green));
					canbus.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.canbus_green, 0, 0);
				}
				if(gpsConnection.isBound())
				{
					if(gpsConnection.getService().hasFix())
					{
						gps.setTextColor(getResources().getColor(R.color.neon_green));
						gps.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.canbus_green, 0, 0);
					}
					else
					{
						gps.setTextColor(getResources().getColor(R.color.bright_yellow));
						gps.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.canbus_yellow, 0, 0);
					}
				}
				else
				{
					gps.setTextColor(getResources().getColor(R.color.bright_red));
					gps.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.canbus_red, 0, 0);
				}
			}
		};
		updater.setDelayMultiplier(4);
		updater.setMessages(new String[] {//"t7E340322F00F", "t7E340322F00C",
											"t7E340322F00D", "t7E5403222030"});
		updater.execute();
	}

	public void handleClick(View view)
	{
		updater.cancel(true);
		while(!updater.isCancelled())
			try {Thread.sleep(50);}
			catch (InterruptedException e) {e.printStackTrace();}
		MightyBlueStatusFragment frag = null;
		switch(view.getId())
		{
			case R.id.driving_controller_status:
				frag = new ControllerStatusFragment();
				break;
			case R.id.driving_bms_status:
				frag = new BMSStatusFragment();
				break;
			case R.id.driving_gps_status:
				frag = new GPSStatusFragment();
				break;
		}
		if(frag == null)
			return;
		cancel();
		frag.setTargetFragment(this, 0);
		frag.show(getFragmentManager(), "drivingStatus");
	}

	@Override
	public void cancel()
	{
		//Yes I cancelled the updater variable goes here
		if(!isCancelled())
			updater.cancel(true);
	}

	@Override
	public boolean isCancelled()
	{
		return updater.isCancelled();
	}
}
