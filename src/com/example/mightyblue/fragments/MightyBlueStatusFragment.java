package com.example.mightyblue.fragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import com.example.mightyblue.services.CanbusServiceConnection;
import com.example.mightyblue.services.CanbusUpdaterService;

public class MightyBlueStatusFragment extends DialogFragment
{
	protected CanbusServiceConnection mConnection = new CanbusServiceConnection();

	@Override
	public void onResume()
	{
		super.onResume();
		Intent intent = new Intent(getActivity(), CanbusUpdaterService.class);
        getActivity().bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        createNewUpdater();
	}

	public void createNewUpdater() {}
	public void cancel() {}

	@Override
	public void onPause()
	{
		cancel();
        if (mConnection.isBound())
            getActivity().unbindService(mConnection);
        super.onPause();
	}
	
    protected class UpdateUI extends AsyncTask<Void, CanbusServiceConnection, Void> 
    {
    	private long sleepTime = 50; 
		@Override
		protected Void doInBackground(Void... arg0)
		{
			while (!isCancelled()) {
				publishProgress(mConnection);
				try {
					Thread.sleep(sleepTime);
				} catch (InterruptedException e) {}
			}
			return null;
		}
		public void setSleepTime(long sleepTime)
		{
			this.sleepTime = sleepTime;
		}
		@Override
		protected void onPreExecute() {}
		@Override
		protected void onPostExecute(Void params) {}
		@Override
		protected void onProgressUpdate(CanbusServiceConnection... connection) {}	
		@Override
		protected void onCancelled() {}
	}

    @Override
	public Dialog onCreateDialog(Bundle savedInstanceState)
	{
		super.onCreateDialog(savedInstanceState);
		return null;
	}

	public boolean isCancelled() {return true;}
}
