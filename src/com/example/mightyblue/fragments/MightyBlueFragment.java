package com.example.mightyblue.fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import com.example.mightyblue.services.CanbusServiceConnection;
import com.example.mightyblue.services.CanbusUpdaterService;

public class MightyBlueFragment extends Fragment
{
	protected CanbusServiceConnection mConnection = new CanbusServiceConnection();

	@Override
	public void onResume()
	{
		super.onResume();
		Intent intent = new Intent(getActivity(), CanbusUpdaterService.class);
        getActivity().bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        createNewUpdater();
	}

	public void createNewUpdater() {}
	public void cancel() {}
	public boolean isCancelled() {return true;}

	@Override
	public void onPause()
	{
		cancel();
        if (mConnection.isBound())
            getActivity().unbindService(mConnection);
        super.onPause();
	}
	
    protected class UpdateUI extends AsyncTask<Void, CanbusServiceConnection, Void> 
    {
    	private int delayMultiplier = 4;
    	private String[] myMessages = null;

		@Override
		protected Void doInBackground(Void... arg0)
		{
			int writeCounter = delayMultiplier+1;
			while (!isCancelled()) {
				if(!isAdded())
					return null;
				if(mConnection.isBound())
				{
					writeCounter += 1;
					if(writeCounter > delayMultiplier)
					{
						writeCounter = 0;
						if(myMessages != null)
							for(int i=0;i<myMessages.length;++i)
								mConnection.getService().write(myMessages[i]);
					}
				}
				publishProgress(mConnection);
				try {
					Thread.sleep(50);
				} catch (InterruptedException e) {}
			}
			return null;
		}
		protected void setDelayMultiplier(int delay)
		{
			this.delayMultiplier = delay;
		}
		protected void setMessages(String[] mesgs)
		{
			this.myMessages = mesgs;
		}
		@Override
		protected void onPreExecute() {}
		@Override
		protected void onPostExecute(Void params) {}
		@Override
		protected void onProgressUpdate(CanbusServiceConnection... connection) {}	
		@Override
		protected void onCancelled() {}	
	}
}
