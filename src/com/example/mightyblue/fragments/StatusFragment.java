package com.example.mightyblue.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.mightyblue.CanbusVariables;
import com.example.mightyblue.R;
import com.example.mightyblue.services.CanbusServiceConnection;

public class StatusFragment extends MightyBlueFragment
{
	TextView amps;
	TextView volts;
	TextView templ;
	TextView temph;
	TextView soc;
	TextView health;
	UpdateUI updater;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View v = inflater.inflate(R.layout.status, container, false);
		amps = (TextView) v.findViewById(R.id.status_amps);
		volts = (TextView) v.findViewById(R.id.status_volts);
		templ = (TextView) v.findViewById(R.id.status_templ);
		temph = (TextView) v.findViewById(R.id.status_temph);
		soc = (TextView) v.findViewById(R.id.status_soc);
		health = (TextView) v.findViewById(R.id.status_health);
		Typeface type = Typeface.createFromAsset(getActivity().getAssets(),"Digital7.ttf");
		amps.setTypeface(type);
		volts.setTypeface(type);
		templ.setTypeface(type);
		temph.setTypeface(type);
		soc.setTypeface(type);
		health.setTypeface(type);
		return v;
	}

	@Override
	public void createNewUpdater()
	{
		updater = new UpdateUI()
		{
			@Override
			protected void onProgressUpdate(CanbusServiceConnection... connection)
			{
				if(!mConnection.isBound())
					return;
				CanbusVariables vars = connection[0].getService().getVariables();
				amps.setText(vars.findVariable("B_PACK_CURRENT").getValue() + "");
				volts.setText(vars.findVariable("B_PACK_VOLTAGE").getValue() + "");
				temph.setText(String.format(".2f", vars.findVariable("B_HIGH_TEMP").getValue()));
				templ.setText(String.format(".2f", vars.findVariable("B_LOW_TEMP").getValue()));
				soc.setText(vars.findVariable("B_PACK_SOC").getValue() + "");
				health.setText(vars.findVariable("B_PACK_HEALTH").getValue() + "");
			}
		};
		updater.execute();
	}

	@Override
	public void cancel()
	{
		if(!updater.isCancelled())
			updater.cancel(true);
	}

	@Override
	public boolean isCancelled()
	{
		return updater.isCancelled();
	}
}
