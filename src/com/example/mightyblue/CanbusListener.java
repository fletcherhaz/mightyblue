package com.example.mightyblue;

import android.util.Log;

import com.hoho.android.usbserial.util.SerialInputOutputManager;

public class CanbusListener implements SerialInputOutputManager.Listener
{
	private static final String TAG = "MightyBlue-CL";

	private CanbusVariables vars;

	public CanbusListener(CanbusVariables vars)
	{
		this.vars = vars;
	}
	
    @Override
    public void onRunError(Exception e) {
        Log.d(TAG, "Runner stopped.");
    }

    @Override
    public void onNewData(final byte[] data) {
    	String test = new String(data);
    	if(!(test.startsWith("t") || test.startsWith("x")))
    	{
    		//System.out.println(test + "TESTING");
    		return;
    	}
    	test = test.trim();
        String temp[] = test.split("(?=[tx])");
        String validChars = "0123456789ABCDEFtx";
        String sid = "";
        String mesg = "";
        String chunk = "";
        String tmp = "";
        String tmp2 = "";

        for(int i = 0; i < temp.length; ++i)
        {
        	try
        	{
        		// TODO: Fix this mess
				tmp = temp[i].trim();
				tmp2 = temp[i].trim();
				for (int j = 0; j < tmp.length(); ++j)
					if (validChars.indexOf(tmp.charAt(j)) == -1) {
						//Log.e(TAG, tmp + "\n" + tmp2 + "\n" + j + "\n" + tmp2.length());
						if(tmp.charAt(j) == '`')
							tmp = tmp.substring(0, j-1);
						else
							tmp = tmp.substring(0, j-1);
						if(j == tmp2.length()-1)
							tmp += tmp2.substring(j, tmp2.length()-1);
						else
							tmp += tmp2.substring(j+1, tmp2.length()-1);
					}
				tmp2 = tmp.trim();
				if(tmp.equals("t")) continue;
				if (tmp.length() % 2 == 0 && tmp.startsWith("x")) {
					sid = tmp.substring(1, 9);
					mesg = tmp2.substring(9);
				}
				else if (tmp.length() % 2 != 0 && tmp.startsWith("t")) {
					sid = tmp.substring(1, 4);
					mesg = tmp2.substring(4);
				}

				if(sid.equals("") || mesg.equals("")) continue;
				if(!sid.equals("709") && !sid.equals("708"))
					Log.i("MightyBlue-------", tmp);
				int ecuId = Integer.parseInt(sid, 16);
				if(ecuId == 2029 || ecuId == 2027)
				{
					// Ex: 80462F00F79000000					
					if (mesg.length() >= 9) {
						chunk = mesg.substring(5, 9);
						int pid = Integer.parseInt(chunk, 16);
						mesg = mesg.substring(9, mesg.length());

						// Update variables
						vars.updateMessage(0, pid, mesg);
					}
				}
				else
				{
					vars.updateMessage(ecuId, 0, mesg);
				}
        	} catch(Exception e)
        	{
        		Log.e(TAG, tmp + "\n" + tmp2);
        		e.printStackTrace();
        		Log.e(TAG, "Skipping message because exception occured: " + tmp);
        	}
        }
    }
}
