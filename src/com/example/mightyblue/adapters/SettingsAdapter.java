package com.example.mightyblue.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.example.mightyblue.R;
 
public class SettingsAdapter extends ArrayAdapter<String> {

	private String[] names;
    private LayoutInflater mInflater;
    private SharedPreferences prefs;

    public SettingsAdapter(Context context, int textViewResourceId, String[] names) {
    	super(context, textViewResourceId, names);
    	this.names = names;
    	mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    	prefs = context.getSharedPreferences("MightyBlueGPS", Context.MODE_PRIVATE);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
    	View v = mInflater.inflate(R.layout.settingsrow, null);
    	((TextView) v.findViewById(R.id.setting_name)).setText(names[position]);
		Switch toggle = (Switch) v.findViewById(R.id.setting_toggle);
		toggle.setChecked(prefs.getBoolean(names[position] + "Setting", false));
		toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
		    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
	        	prefs.edit().putBoolean(names[position] + "Setting", isChecked).apply();
		    }
		});
        return v;
    }

}