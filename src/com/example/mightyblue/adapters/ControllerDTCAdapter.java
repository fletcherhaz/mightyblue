package com.example.mightyblue.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.mightyblue.CanbusVariables;
import com.example.mightyblue.R;
import com.example.mightyblue.Variable;
 
public class ControllerDTCAdapter extends ArrayAdapter<String> {

    private int num_errors;
    private String[][] dtcs = new String[22][2];
    private LayoutInflater mInflater;

    public ControllerDTCAdapter(Context context, int textViewResourceId, String[] names) {
    	super(context, textViewResourceId, names);
    	mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void updateDTCs(CanbusVariables vars)
    {
		Variable[] var = new Variable[22];
		String[] names = new String[22];
		num_errors = 0;
		for(int i=0; i<22; ++i)
		{
			var[i] = vars.findVariable("M_DTC_" + String.format("%02d",  i+1));
			if (var[i].getValue() == 1)
			{
				dtcs[num_errors][0] = "Motor DTC " + (i+1);
				names[num_errors] = "Motor DTC " + (i+1);
				dtcs[num_errors][1] = var[i].getDescription();
				++num_errors;
			}
		}
		int tmp_count = num_errors;
		for(int i=0; i<22; ++i)
		{
			if (var[i].getValue() == 0)
			{
				dtcs[tmp_count][0] = "Motor DTC " + (i+1);
				names[tmp_count] = "Motor DTC " + (i+1);
				dtcs[tmp_count][1] = var[i].getDescription();
				++tmp_count;
			}
		}
		this.notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
    	ViewHolder holder;
        if (convertView == null) {
        	convertView = mInflater.inflate(R.layout.controllerdtcrow, null);
        	holder = new ViewHolder();
        	holder.name = (TextView) convertView.findViewById(R.id.controllerdtc_name);
        	holder.desc = (TextView) convertView.findViewById(R.id.controllerdtc_desc);
        	//uholder.name.setBackgroundColor(parent.getResources().getColor(R.color.black));
        	//textView.setTextSize(14);
        	convertView.setTag(holder);
        }
        else
        {
        	holder = (ViewHolder) convertView.getTag();
        }
        if(position < num_errors)
        {
        	holder.name.setTextColor(parent.getResources().getColor(R.color.bright_red));
        	holder.desc.setTextColor(parent.getResources().getColor(R.color.bright_red));
        }
        else
        {
        	holder.name.setTextColor(parent.getResources().getColor(R.color.neon_green));
        	holder.desc.setTextColor(parent.getResources().getColor(R.color.neon_green));
        }
        holder.name.setText(dtcs[position][0]);
        holder.desc.setText(dtcs[position][1]);
        return convertView;
    }
        
        class ViewHolder {
            TextView name;
            TextView desc;
        }

}