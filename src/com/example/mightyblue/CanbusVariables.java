package com.example.mightyblue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import org.xmlpull.v1.XmlPullParser;

import android.content.res.Resources;

import com.primalworld.math.MathEvaluator;

public class CanbusVariables {
    private HashMap<String, Variable> variables;
    private HashMap<Integer, ArrayList<String>> variablesById;
    public MathEvaluator mex; // MathEvaluator instance for expression

    // Constructor to initialize variable list
    public CanbusVariables(Resources res) {
		variables = new HashMap<String, Variable>();
		variablesById = new HashMap<Integer, ArrayList<String>>();
        
		// Create new math evaluator object
		mex = new MathEvaluator();
		
		// Load variable configuration file
		loadVariables(res);
    }
    
    // Update a variable based on a recent CANBUS message
    public void updateMessage(int mesgid, int pid, String mesg)
    {
    	int id = 0;
    	if(mesgid == 0) id = pid;
    	if(pid == 0) id = mesgid;
    	//If we don't use this message
    	if(!variablesById.containsKey(id))
    		return;
        // Loop through all variables in list
    	for(String varName: variablesById.get(id))
    	{
            Variable var = variables.get(varName);
            var.updateValue(mesg, mex);
        }
    }

    // Find a variable and return a reference to it
    public Variable findVariable(String name) {
        Variable var = variables.get(name);
        if(var == null)
        {
        	var = Variable.createDummyVariable();
        }
		return var;
    }

    // Load variables from configuration XML file
    private void loadVariables(Resources res) {
		try {
			XmlPullParser xml = res.getXml(R.xml.variables);
			HashSet<String> names = new HashSet<String>();
			while (xml.getEventType() != XmlPullParser.END_DOCUMENT) {
				if (xml.getEventType() == XmlPullParser.START_TAG) {
					if (xml.getName().equals("VARIABLES")) {
						xml.next();
						continue;
					}
					Variable v = new Variable();
					for (int i = 0; i < xml.getAttributeCount(); ++i) {
						if (xml.getAttributeName(i).equals("NAME")) {
							v.setName(xml.getAttributeValue(i));
						} else if (xml.getAttributeName(i).equals("MESGID")) {
							v.setMesgid(Integer.parseInt(
									xml.getAttributeValue(i), 16));
						} else if (xml.getAttributeName(i).equals("LENGTH")) {
							v.setLength(Integer.parseInt(xml
									.getAttributeValue(i)));
						} else if (xml.getAttributeName(i).equals("EXPRESSION")) {
							v.setExpression(xml.getAttributeValue(i));
						} else if (xml.getAttributeName(i).equals("BIT")) {
							v.setBit(xml.getAttributeValue(i));
						}
					}
					if (v.getName().equals("")) {
						System.err.println("loadVariables: Error - Malformed XML file");
						xml.next();
						continue;
					}
					if (names.contains(v.getName())) {
						System.err.println("ERROR: loadVariables - duplicate variable for: " + v.getName());
						xml.next();
						continue;
					}
					names.add(v.getName());
					mex.addVariable(v.getName(), 1);
					variables.put(v.getName(), v);
					if(variablesById.containsKey(v.getMesgid()))
					{
						ArrayList<String> tmp = variablesById.get(v.getMesgid());
						tmp.add(v.getName());
						variablesById.put(v.getMesgid(), tmp);
					}
					else
					{
						ArrayList<String> tmp = new ArrayList<String>();
						tmp.add(v.getName());
						variablesById.put(v.getMesgid(), tmp);
					}
				}
				xml.next();
			}
		} catch (Exception e) {
			System.err.println("Exception: " + e.getMessage());
			e.printStackTrace();
		}
    }
}
