package com.example.mightyblue.services;

import java.io.File;
import java.io.FileWriter;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.os.SystemClock;
import android.util.Log;

public class GPSUpdaterService extends Service implements LocationListener, GpsStatus.Listener
{
    private static final long MIN_TIME = 500;
    private String speed = "0";
    private Location mLastLocation;
    private long mLastLocationMillis = 0;
    private boolean isGPSFix = false;
    private double[] latData = new double[20];
    private double[] lngData = new double[20];
    private double[] speedData = new double[20];
    private int gpsStored = 0;
	private boolean mExternalStorageWriteable = false;
	private SharedPreferences prefs;

    public class GPSServiceBinder extends Binder {
        GPSUpdaterService getService() {
            // Return this instance of CanbusUpdater so clients can call public methods
            return GPSUpdaterService.this;
        }
    }
    private GPSServiceBinder gpsBinder = new GPSServiceBinder();
    
	@Override
	public int onStartCommand(Intent intent, int flags, int startId)
	{
		prefs = getSharedPreferences("MightyBlueGPS", Context.MODE_PRIVATE);
		LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		boolean enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
		if (enabled) {
			locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME, 0, this);
			locationManager.addGpsStatusListener(this);
			Log.i("GPSUpdater", "START THE GPS UPDATER");
			speed = "0";
			String state = Environment.getExternalStorageState();
			if (Environment.MEDIA_MOUNTED.equals(state)) {
		    	mExternalStorageWriteable = true;
			} else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
		    	mExternalStorageWriteable = false;
			} else {
				mExternalStorageWriteable = false;
			}
		}
	    return START_REDELIVER_INTENT;
	}

	@Override
	public void onLocationChanged(Location loc)
	{
		long total = prefs.getLong("TotalMiles", 0);
		float trip = prefs.getFloat("TripMiles", 0);
		int oldTrip = (int) trip;
		if(mLastLocation != null)
			trip += (loc.distanceTo(mLastLocation)*0.000621371);
		if(oldTrip < (int) trip)
			total += ((int) trip) - oldTrip;
		prefs.edit().putLong("TotalMiles", total).apply();
		prefs.edit().putFloat("TripMiles", trip).apply();
		if(gpsStored == 20)
		{
		    try
		    {
		    	File file = new File(getExternalFilesDir(null), "gpsdata0.txt");
		    	FileWriter fos = new FileWriter(file, false);
		    	if(mExternalStorageWriteable){
		    		for(int i =0; i< 20; ++i)
		    		{
		    			fos.write(latData[i] + ",");
		    			fos.write(lngData[i] + ",");
		    			fos.write(speedData[i] + "\n");
		    		}
		    	}
		    	fos.close();
		    }
		    catch (Exception e)
		    {
		    	e.printStackTrace();
		    }
		    gpsStored = 0;
		}
		double lat = (double) (loc.getLatitude());
		double lng = (double) (loc.getLongitude());
		speed = loc.getSpeed()*2.23694 + "";
	    mLastLocationMillis = SystemClock.elapsedRealtime();
	    mLastLocation = loc;
	    latData[gpsStored] = lat;
	    lngData[gpsStored] = lng;
	    speedData[gpsStored] = loc.getSpeed()*2.23694;
	    gpsStored+=1;
//	    Log.i("MightyBlue-GU", gpsStored + "");
	}

	@Override
	public void onProviderDisabled(String provider)
	{
		Log.e("MightyBlue", "GPS Disabled");
	}

	@Override
	public void onProviderEnabled(String provider)
	{
		Log.e("MightyBlue", "GPS Enabled");
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras)
	{
		//speed.append(":" + locationManager.getGpsStatus(null).getTimeToFirstFix());
	}

	@Override
	public void onGpsStatusChanged(int event)
	{
		switch (event) {
            case GpsStatus.GPS_EVENT_SATELLITE_STATUS:
                if (mLastLocation != null)
                    isGPSFix = (SystemClock.elapsedRealtime() - mLastLocationMillis) < 3000;
                break;
            case GpsStatus.GPS_EVENT_FIRST_FIX:
                isGPSFix = true;
                break;
        }
	}

    @Override
    public IBinder onBind(Intent intent) {
        // A client is binding to the service with bindService()
        return gpsBinder;
    }
    @Override
    public boolean onUnbind(Intent intent) {
        // All clients have unbound with unbindService()
        return true;
    }
    @Override
    public void onRebind(Intent intent) {}

    @Override
    public void onDestroy() {
    	((LocationManager) getSystemService(Context.LOCATION_SERVICE)).removeGpsStatusListener(this);
    	((LocationManager) getSystemService(Context.LOCATION_SERVICE)).removeUpdates(this);
    }

	public boolean hasFix()
	{
		return isGPSFix;
	}

	public String getSpeed()
	{
		return speed;
	}

}