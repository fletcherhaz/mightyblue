package com.example.mightyblue.services;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;

import com.example.mightyblue.services.GPSUpdaterService.GPSServiceBinder;

public class GPSServiceConnection implements ServiceConnection
{
	private boolean gpsBound = false;
	private GPSUpdaterService gpsService = null;

    @Override
    public void onServiceConnected(ComponentName className, IBinder service) {
    	if(service.isBinderAlive())
    	{
    		GPSServiceBinder binder = (GPSServiceBinder) service;
    		gpsService = binder.getService();
    		gpsBound = true;
    	}
    	else
    	{
    		gpsBound = false;
    	}
    }

    @Override
    public void onServiceDisconnected(ComponentName arg0) {
        gpsBound = false;
    }

    public boolean isBound()
    {
    	return gpsBound;
    }

    public GPSUpdaterService getService()
    {
    	return gpsService;
    }
}
