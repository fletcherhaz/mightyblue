package com.example.mightyblue.services;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.usb.UsbManager;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import com.example.mightyblue.CanbusListener;
import com.example.mightyblue.CanbusVariables;
import com.hoho.android.usbserial.driver.UsbSerialDriver;
import com.hoho.android.usbserial.driver.UsbSerialProber;
import com.hoho.android.usbserial.util.SerialInputOutputManager;

public class CanbusUpdaterService extends Service
{
	private static final String TAG = "MightyBlue-CU";

    private UsbManager mUsbManager;
    private final ExecutorService mExecutor = Executors.newSingleThreadExecutor();

    private UsbSerialDriver mSerialDevice;
    private SerialInputOutputManager mSerialIoManager;

    private CanbusVariables vars;
    private CanbusListener mListener;

    public class CanbusServiceBinder extends Binder {
        CanbusUpdaterService getService() {
            // Return this instance of CanbusUpdater so clients can call public methods
            return CanbusUpdaterService.this;
        }
    }
    private CanbusServiceBinder mBinder = new CanbusServiceBinder();

	@Override
	public int onStartCommand(Intent intent, int flags, int startId)
	{
		initialize();
        return START_REDELIVER_INTENT;
	}

	private void initialize()
	{
		vars = new CanbusVariables(getResources());
		mListener = new CanbusListener(vars);
		mUsbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
		mSerialDevice = UsbSerialProber.acquire(mUsbManager);
        Log.d(TAG, "Resumed, mSerialDevice=" + mSerialDevice);
        if (mSerialDevice != null)
        {
            try {
                mSerialDevice.open();
                mSerialDevice.setParameters(921600, UsbSerialDriver.DATABITS_8, UsbSerialDriver.STOPBITS_1, UsbSerialDriver.PARITY_NONE);
                mSerialDevice.getConnection().controlTransfer(0, 0, 0, 0, null, 0, 0);//reset
                mSerialDevice.getConnection().controlTransfer(0, 0, 1, 0, null, 0, 0);//clear Rx
                mSerialDevice.getConnection().controlTransfer(0, 0, 2, 0, null, 0, 0);//clear Tx
                String messageString = "\015\015\015";
                mSerialDevice.write(messageString.getBytes(), 200);
                messageString = "S5\015";
                mSerialDevice.write(messageString.getBytes(), 200);
                messageString = "O\015";
                mSerialDevice.write(messageString.getBytes(), 200);
                messageString = "Z0\015";
                mSerialDevice.write(messageString.getBytes(), 200);
            } catch (IOException e) {
                Log.e(TAG, "Error setting up device: " + e.getMessage(), e);
                try {
                    mSerialDevice.close();
                } catch (IOException e2) {
                    // Ignore.
                }
                mSerialDevice = null;
                return;
            }
            Log.d(TAG, mSerialDevice.getDevice().getDeviceName());
        }
        stopIoManager();
        startIoManager();
	}

	private void stopIoManager() {
        if (mSerialIoManager != null) {
            Log.i(TAG, "Stopping io manager ..");
            mSerialIoManager.stop();
            mSerialIoManager = null;
        }
    }

	private void startIoManager() {
        if (mSerialDevice != null) {
            Log.i(TAG, "Starting io manager ..");
            mSerialIoManager = new SerialInputOutputManager(mSerialDevice, mListener);
            mExecutor.submit(mSerialIoManager);
        }
    }
	
    @Override
    public IBinder onBind(Intent intent) {
        // A client is binding to the service with bindService()
        return mBinder;
    }
    @Override
    public boolean onUnbind(Intent intent) {
        // All clients have unbound with unbindService()
        return true;
    }
    @Override
    public void onRebind(Intent intent) {}

    @Override
    public void onDestroy() {
        stopIoManager();
        if (mSerialDevice != null) {
            try {
                mSerialDevice.close();
            } catch (IOException e) {
                // Ignore.
            }
            mSerialDevice = null;
        }
    }

    public CanbusVariables getVariables()
    {
    	return vars;
    }

    public boolean usbConnected()
    {
    	if(mSerialDevice != null) return true;
    	else return false;
    }

    public void write(String mesg)
    {
    	mesg = mesg + "\006";
    	if(mSerialIoManager != null)
			mSerialIoManager.writeAsync(mesg.getBytes());
    }
}
