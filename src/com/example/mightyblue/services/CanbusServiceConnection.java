package com.example.mightyblue.services;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;

import com.example.mightyblue.services.CanbusUpdaterService.CanbusServiceBinder;

public class CanbusServiceConnection implements ServiceConnection
{
	private boolean mBound = false;
	private CanbusUpdaterService mService = null;

    @Override
    public void onServiceConnected(ComponentName className, IBinder service) {
        // We've bound to LocalService, cast the IBinder and get LocalService instance
        CanbusServiceBinder binder = (CanbusServiceBinder) service;
        mService = binder.getService();
        mBound = true;
    }

    @Override
    public void onServiceDisconnected(ComponentName arg0) {
        mBound = false;
    }

    public boolean isBound()
    {
    	return mBound;
    }

    public CanbusUpdaterService getService()
    {
    	return mService;
    }
}
