package com.example.mightyblue;

import com.primalworld.math.MathEvaluator;

public class Variable {
	
	private static String ALPHAS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	
    private String name;        // Variable name
    private String expression;  // Arithmetic expression for variable
    private int mesgid;         // Message ID for corresponding message
    private int length;         // Length of data
    private double value;       // Value of variable
    private int bit;

    public Variable() {
        this.name = "";
        this.mesgid = 0;
        this.value = 0;
        this.expression = "";
        this.bit = -1;
    }

    // Update the value of a variable
    public void updateValue(String mesg, MathEvaluator mex) {
    	int numVars = expression.length()-expression.replaceAll("[A-Z]", "").length();
        long[] newvals = new long[numVars];
    	for (int i = 1; i <= numVars; ++i) {
    		newvals[i-1] = Integer.parseInt(mesg.substring(i*2*length-2, i*2*length), 16);
	    }

    	if(this.bit != -1)
    		setValue(Integer.parseInt(Long.toBinaryString(
    				newvals[ALPHAS.indexOf(this.expression)]).substring(this.bit, this.bit+1)));
    	else if (this.expression != null && !this.expression.equals(""))
    	{
	        mex.setExpression(this.expression);
	        for (int i = 0; i < newvals.length; ++i)
	        	mex.addVariable(ALPHAS.substring(i, i+1), newvals[i]);
	       
	        try {
	            setValue(mex.getValue());
	        } catch(java.lang.NullPointerException nex) {
	        }
	    }
    	else
    		setValue(newvals[0]);
    	//Log.d(MightyBlue.class.getSimpleName(), name + ": " + newval);
    }
  
    public int getMesgid() {
        return mesgid;
    }
    
    public String getName() {
        return name;
    }

    public double getValue() {
        return value;
    }
    
    public void setValue(double value) {
        this.value = value;
    }

    public void setLength(int length) {
        this.length = length;
    }
    
    public void setMesgid(int mesgid) {
        this.mesgid = mesgid;
    }

    public String getDescription()
    {
    	return "No description available.";
    }
    
    public void setName(String name)
    {
    	this.name = name;
    }

    public void setBit(String bit)
    {
    	this.bit = Integer.parseInt(bit);
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

	public static Variable createDummyVariable()
	{
		Variable var = new Variable();
	    var.setName("Dummy Var");
	    var.setValue(0);
		return var;
	}
}
